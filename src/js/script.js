function validateEmail(emailaddress) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (emailReg.test(emailaddress)) {
        return true;
    }
    return false;
}


$(document).ready(function () {

    var JSAPP = JSAPP || {};

    JSAPP.animateBlocks = function () {
        $('.animated').isInViewport().addClass('inViewport');
    };

    JSAPP.animationInViewport = function () {
        $(window).on('scroll resize', function () {
            JSAPP.animateBlocks();
        })
    };
    JSAPP.animateBlocks();


    JSAPP.carousel = function () {
        $('.owl-carousel').owlCarousel({
            //autoplay: true,
            autoplayHoverPause: true,
            //animateIn: true,
            //animateOut: true,
            loop: true,
            margin: 10,
            dots: true,
            responsive: {
                0: {
                    items: 1
                }
            }
        });
    };

    JSAPP.contatcForm = function () {
        $('#feedback-form').ajaxForm({
            beforeSubmit: function (formData, jqForm, options) {
                var form = jqForm[0];
                var err = false;
                $(form).find('.error').removeClass('error');

                if (!form.name.value) {
                    $(form.name).addClass('error');
                    err = true;
                }
                if (!form.email.value) {
                    $(form.email).addClass('error');
                    err = true;
                } else {
                    if (!validateEmail(form.email.value)) {
                        $(form.email).addClass('error');
                        err = true;
                    }
                }

                if (!form.message.value) {
                    $(form.message).addClass('error');
                    err = true;
                }
                if (err) {
                    return false;
                }
            },
            success: function () {
                $('#contact-us .submit').hide();
                $('#contact-us .success-msg').fadeIn();
                $('#feedback-form input, #feedback-form textarea').not('.submit').val('');
            },
            error: function (e) {
                if (e.status == 200) {
                    $('#contact-us .submit').hide();
                    $('#contact-us .success-msg').fadeIn();
                    $('#feedback-form input, #feedback-form textarea').not('.submit').val('');
                } else {
                    //error
                }
            }
        });
    };

    JSAPP.video = function () {
        var container = $('#video'),
            content = $('.content', container),
            playBtn = $('.play-btn', container),
            src = 'https://www.youtube.com/embed/Qg1BY8n4CuU?autoplay=1&rel=0&controls=0&showinfo=0',
            frame = '<iframe class="img-responsive" id="video-frame" src="https://www.youtube.com/embed/Qg1BY8n4CuU?autoplay=1&rel=0&controls=0&showinfo=0" frameborder="0"  webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" ></iframe>';

        setVideo();

        $(window).on('resize', function() {
            if( $('#video-frame', container) ) {
                setSize();
            }

            // Checking if video is playing
            if( $('.play-btn', container).is(':hidden') ) {
                clearView();
            }
        });

        function setVideo() {
            if ( $(window).innerWidth() <= 768 ) {
                playBtn.attr('href', src);
            } else {
                playBtn.on('click', function () {
                    $('.content', container).prepend(frame);
                    setSize();
                    clearView();

                    $('.play-btn, .video-placeholder, .video-text, .line', container).hide();

                    return false;
                });
            }
        }

        function setSize() {
            var embeddedFrame = $('#video-frame', container),
                embeddedFrameInitialWidth = 960,
                embeddedFrameCurrentWidth,
                embeddedFrameCurrentHeight,
                embeddedFrameRatio = 0.5625;

            if( embeddedFrameInitialWidth >= $(window).innerWidth() ) {
                embeddedFrameCurrentWidth = $(window).innerWidth();
            } else {
                embeddedFrameCurrentWidth = embeddedFrameInitialWidth;
            }

            embeddedFrameCurrentHeight = embeddedFrameCurrentWidth * embeddedFrameRatio;

            embeddedFrame.css({
                'width': embeddedFrameCurrentWidth + 'px',
                'height': embeddedFrameCurrentHeight + 'px'
            });
        }

        function clearView() {
            var animatedBlock = $('.animation-block', '#main');

            if($(window).innerWidth() > 768) {
                animatedBlock.css({
                    transition: 'all 1s ease',
                    transform: 'scale(.9) translateY(-35px)'
                });
            } else {
                animatedBlock.css({
                    transition: 'all 1s ease',
                    transform: 'scale(1) translateY(0)'
                });
            }
        }
    };

    JSAPP.animationInViewport();
    JSAPP.carousel();
    JSAPP.contatcForm();
    JSAPP.video();
});