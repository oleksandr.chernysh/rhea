'use strict';

var gulp = require('gulp'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    rigger = require('gulp-rigger'),
    htmlmin = require('gulp-htmlmin'),
    gulpif = require('gulp-if'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    mainBowerFiles = require("main-bower-files"),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');


var env = process.env.NODE_ENV || 'development';

var path = {
    build: {
        html: 'build/',
        css: 'build/assets/css/',
        js: 'build/assets/js/',
        img: 'build/assets/images/'
    },
    src: {
        html: 'src/html/index.html',
        scss: 'src/scss/style.scss',
        js: 'src/js/script.js',
        img: 'src/images/**/*.*'
    },
    watch: {
        html: 'src/html/index.html',
        htmlTemplates: 'src/html/templates/*.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/images/**/*.*'
    }
};

var browserSyncConfig = {
    server: {
        baseDir: "./build/"
    },
    host: 'localhost',
    port: 9000,
    open: true,
    debug: true
};

var autoPrefConf = {
    browsers: ['last 10 versions']
};


// ============== HTML =================
gulp.task('html', function () {
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulpif(env === 'production', htmlmin()))
        .on('error', gutil.log)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

// ============== CSS =================
gulp.task('css', function () {
    gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(gulpif(env === 'development', sourcemaps.init()))
        .pipe(sass())
        .pipe(autoprefixer(autoPrefConf))
        .pipe(gulpif(env === 'production', cssnano()))
        .pipe(gulpif(env === 'development', sourcemaps.write()))
        .on('error', gutil.log)
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


// ============== JS =================
gulp.task('js', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulpif(env === 'production', uglify()))
        .on('error', gutil.log)
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});


// ============== IMAGES =================
gulp.task('images', function () {
    gulp.src(path.src.img)
        .pipe(plumber())
        .pipe(gulpif(env === 'production', imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()],
            interlaced: true
        })))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});


// ============== VENDORS CSS =================
gulp.task("vendors:css", function () {
    var files = mainBowerFiles({
        filter: /.*\.css$/
    });

    files.push('src/vendors/owl.carousel/dist/assets/owl.theme.default.min.css');
    console.log(files);
    return gulp.src(files)
        .pipe(plumber())
        .pipe(concat('vendors.min.css'))
        .pipe(cssnano())
        .pipe(gulp.dest(path.build.css));
});


// ============== VENDORS JS =================
gulp.task("vendors:js", function () {
    var vfiles = mainBowerFiles({
        filter: /.*\.js$/
    });
    console.log(vfiles);
    return gulp.src(vfiles)
        .pipe(plumber())
        .pipe(concat('vendors.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});


// ============== BUILD =================
gulp.task('build', function () {
    gulp.start('html');
    gulp.start('css');
    gulp.start('js');
    gulp.start('images');
});


// ============== BUILD VENDORS =================
gulp.task('build:vendors', function () {
    gulp.start('vendors:css');
    gulp.start('vendors:js');
});


// ============== SERVER =================
gulp.task('server', function () {
    browserSync(browserSyncConfig);
});


// ============== WATCH =================
gulp.task('watch', function () {
    watch([path.watch.html, path.watch.htmlTemplates], function (event, cb) {
        gulp.start('html');
    });
    watch([path.watch.scss], function (event, cb) {
        gulp.start('css');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('images');
    });
});


// ============== DEFAULT =================
gulp.task('default', ['build', 'server', 'watch']);